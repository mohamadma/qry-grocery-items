package com.sainsburys.digital.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * GroceryItem instance for the application's output. Note the use of Jackson annotations to rename
 * the properties in the output JSON, to order the properties in the output JSON and to omit null
 * properties. An Integer object has been used for caloriesPerHundredGrams rather than an int
 * so that this object can be left as null if it is not available on a web page.
 */
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder({"title", "kcal_per_100g", "unit_price", "description"})
public class GroceryItem {

  @JsonProperty("title")
  private final String title;

  @JsonProperty("kcal_per_100g")
  private final Integer caloriesPerHundredGrams;

  @JsonProperty("unit_price")
  private final double unitPrice;

  @JsonProperty("description")
  private final String description;

  public GroceryItem(String title, Integer caloriesPerHundredGrams, double unitPrice,
      String description) {
    this.title = title;
    this.caloriesPerHundredGrams = caloriesPerHundredGrams;
    this.unitPrice = unitPrice;
    this.description = description;
  }

  public String getTitle() {
    return title;
  }

  public Integer getCaloriesPerHundredGrams() {
    return caloriesPerHundredGrams;
  }

  public double getUnitPrice() {
    return unitPrice;
  }

  public String getDescription() {
    return description;
  }

  @Override
  public String toString() {
    return "GroceryItem{" +
        "title='" + title + '\'' +
        ", caloriesPerHundredGrams=" + caloriesPerHundredGrams +
        ", unitPrice=" + unitPrice +
        ", description='" + description + '\'' +
        '}';
  }
}
