package com.sainsburys.digital.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

/**
 * Response class. Contains list of GroceryItems and the total price of all the items in the list.
 */
public class GroceryItemsResponse {

  @JsonProperty("results")
  private final List<GroceryItem> groceryItems;

  @JsonProperty("total")
  private final double totalPrice;

  public GroceryItemsResponse(
      List<GroceryItem> groceryItems, double totalPrice) {
    this.groceryItems = groceryItems;
    this.totalPrice = totalPrice;
  }

  public List<GroceryItem> getGroceryItems() {
    return groceryItems;
  }

  public double getTotalPrice() {
    return totalPrice;
  }
}
