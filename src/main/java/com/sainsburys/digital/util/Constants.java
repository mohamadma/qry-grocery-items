package com.sainsburys.digital.util;

/**
 * Constants class for the application.
 */
public class Constants {

  // Private constructor to prevent class from being instantiated
  private Constants() {}

  public static final String LINK_HTML_TAG = "a";
  public static final String ABSOLUTE_HREF_TAG = "abs:href";
  public static final String GROCERY_ITEM_TITLE_DIV_NAME = "div.productTitleDescriptionContainer";
  public static final String HEADING_TAG = "h1";
  public static final String GROCERY_ITEM_DESCRIPTION_DIV_NAME = "div.productText";
  public static final String PARAGRAPH_TAG = "p";
  public static final String GROCERY_ITEM_CALORIES_DIV_NAME = "td.nutritionLevel1";
  public static final String N_CALORIES_REGEX = "[0-9]+kcal";
  public static final String CALORIES_REGEX = "kcal";
  public static final String BLANK = "";
  public static final String PRICE_PER_UNIT_DIV_NAME = "p.pricePerUnit";
  public static final String NON_DIGIT_REGEX = "\\D";
  public static final String GRID_ITEM_DIV_NAME = "li.GridItem";

}
