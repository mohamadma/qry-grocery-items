package com.sainsburys.digital.service;

import com.sainsburys.digital.domain.GroceryItemsResponse;
import java.io.IOException;

/**
 * Service class for getting grocery items. Takes in a URL and returns a populated GroceryItemsResponse
 * instance.
 */
public interface GroceryItemsService {

  GroceryItemsResponse getGroceryItemsFromUrl(String url) throws IOException;

}
