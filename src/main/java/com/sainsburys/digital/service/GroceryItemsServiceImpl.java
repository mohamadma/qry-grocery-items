package com.sainsburys.digital.service;

import static com.sainsburys.digital.util.Constants.GRID_ITEM_DIV_NAME;

import com.sainsburys.digital.adapter.GroceryItemAdapter;
import com.sainsburys.digital.domain.GroceryItem;
import com.sainsburys.digital.domain.GroceryItemsResponse;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * Implementation of the GroceryItemsService. Populates the response by getting the URL of each
 * individual item on the catalogue's URL and parsing it into a GroceryItem and calculating
 * the total price of the items.
 */
@Service
public class GroceryItemsServiceImpl implements GroceryItemsService {

  private static final Logger LOG = LoggerFactory.getLogger(GroceryItemsServiceImpl.class);

  private final GroceryItemAdapter groceryItemAdapter;

  public GroceryItemsServiceImpl(
      GroceryItemAdapter groceryItemAdapter) {
    this.groceryItemAdapter = groceryItemAdapter;
  }

  @Override
  public GroceryItemsResponse getGroceryItemsFromUrl(final String url) throws IOException {
    final Document webPageDocument = getWebPageDocument(url);
    final Elements webPageElements = webPageDocument.select(GRID_ITEM_DIV_NAME);

    final List<GroceryItem> groceryItemList = webPageElements.stream()
        .map(groceryItemAdapter::elementToGroceryItemList)
        .collect(Collectors.toList());

    final double totalPrice = groceryItemList.stream()
        .map(GroceryItem::getUnitPrice)
        .mapToDouble(Double::doubleValue)
        .sum();

    return new GroceryItemsResponse(groceryItemList, totalPrice);
  }

  private Document getWebPageDocument(final String url) throws IOException {
    try {
      return Jsoup.connect(url).get();
    } catch (IOException exception) {
      LOG.error(
          "IOException thrown while getting web page contents from %s. Check url is valid and try again.",
          url);
      throw exception;
    }
  }
}
