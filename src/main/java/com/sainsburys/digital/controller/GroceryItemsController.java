package com.sainsburys.digital.controller;

import com.sainsburys.digital.domain.GroceryItemsResponse;
import com.sainsburys.digital.service.GroceryItemsService;
import java.io.IOException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for the application. API consumers will interact with this class via an endpoint such
 * as PostMan or AWS.
 */
@RestController
@RequestMapping("/groceryItems")
public class GroceryItemsController {

  private static final Logger LOG = LoggerFactory.getLogger(GroceryItemsController.class);

  @Value("${sainsburys.catalogue.url}")
  private String catalogueUrl;

  private final GroceryItemsService groceryItemsService;

  public GroceryItemsController(
      GroceryItemsService groceryItemsService) {
    this.groceryItemsService = groceryItemsService;
  }

  @GetMapping(value="/getAll")
  public GroceryItemsResponse getGroceryItems() throws IOException {
    LOG.info("Calling grocery item service with url %s", catalogueUrl);
    return groceryItemsService.getGroceryItemsFromUrl(catalogueUrl);
  }

}
