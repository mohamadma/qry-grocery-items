package com.sainsburys.digital.adapter;

import com.sainsburys.digital.domain.GroceryItem;
import org.jsoup.nodes.Element;

/**
 * Converts a JSoup Element into a GroceryItem instance. Required to parse the content on the website
 */
public interface GroceryItemAdapter {

  GroceryItem elementToGroceryItemList(Element document);

}
