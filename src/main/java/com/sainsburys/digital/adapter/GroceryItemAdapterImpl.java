package com.sainsburys.digital.adapter;

import static com.sainsburys.digital.util.Constants.ABSOLUTE_HREF_TAG;
import static com.sainsburys.digital.util.Constants.BLANK;
import static com.sainsburys.digital.util.Constants.CALORIES_REGEX;
import static com.sainsburys.digital.util.Constants.GROCERY_ITEM_CALORIES_DIV_NAME;
import static com.sainsburys.digital.util.Constants.GROCERY_ITEM_DESCRIPTION_DIV_NAME;
import static com.sainsburys.digital.util.Constants.GROCERY_ITEM_TITLE_DIV_NAME;
import static com.sainsburys.digital.util.Constants.HEADING_TAG;
import static com.sainsburys.digital.util.Constants.LINK_HTML_TAG;
import static com.sainsburys.digital.util.Constants.NON_DIGIT_REGEX;
import static com.sainsburys.digital.util.Constants.N_CALORIES_REGEX;
import static com.sainsburys.digital.util.Constants.PARAGRAPH_TAG;
import static com.sainsburys.digital.util.Constants.PRICE_PER_UNIT_DIV_NAME;

import com.sainsburys.digital.domain.GroceryItem;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.math3.util.Precision;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Implementation of GroceryItemAdapter class. Converts a JSoup Element into a GroceryItem instance.
 * Note that the element that is passed in must be of div type li.GridItem from the Sainsbury's
 * online catalogue.
 */

@Component
public class GroceryItemAdapterImpl implements GroceryItemAdapter {

  private static final Logger LOG = LoggerFactory.getLogger(GroceryItemAdapterImpl.class);

  @Override
  public GroceryItem elementToGroceryItemList(final Element element) {

    try {
      final String itemUrl = element.select(LINK_HTML_TAG).attr(ABSOLUTE_HREF_TAG);
      LOG.info("Getting web document from URL: {}", itemUrl);
      final Document itemWebDocument = Jsoup.connect(itemUrl).get();
      final String title = extractTitleFromDocument(itemWebDocument);
      final Integer kCalPerHundredGrams = extractCaloriesFromDocument(itemWebDocument);
      final double unitPrice = extractUnitPriceFromDocument(itemWebDocument);
      final String description = extractDescriptionFromDocument(itemWebDocument);

      final GroceryItem groceryItem = new GroceryItem(title, kCalPerHundredGrams, unitPrice, description);
      LOG.info("Parsed grocery item: {}\n", groceryItem);
      return groceryItem;
    }

    catch (IOException exception) {
      LOG.error("IOException thrown while getting data from the web: {}, returning null", exception);
      return null;
    }
  }

  private String extractTitleFromDocument(final Document itemWebDocument) {
    final Elements productTitleDescriptionContainer = itemWebDocument
        .select(GROCERY_ITEM_TITLE_DIV_NAME);
    return productTitleDescriptionContainer.select(HEADING_TAG).first().text();
  }

  private Integer extractCaloriesFromDocument(final Document itemWebDocument) {
    int kCalPerHundredGrams;
    final Elements kCalElements = itemWebDocument.select(GROCERY_ITEM_CALORIES_DIV_NAME);
    final Pattern kCalRegexPattern = Pattern.compile(N_CALORIES_REGEX);
    final Matcher matcher = kCalRegexPattern.matcher(kCalElements.text());
    if (matcher.find()) {
      String kCalPerHundredGramsString = matcher.group(0).replaceAll(CALORIES_REGEX, BLANK);
      kCalPerHundredGrams = Integer.parseInt(kCalPerHundredGramsString);
      return kCalPerHundredGrams;
    } else {
      return null;
    }
  }

  private double extractUnitPriceFromDocument(final Document itemWebDocument) {
    final Elements pricePerUnitElement = itemWebDocument.select(PRICE_PER_UNIT_DIV_NAME);
    final double priceInPence = Double
        .parseDouble(pricePerUnitElement.first().text().replaceAll(NON_DIGIT_REGEX, BLANK));
    return convertPenceToPounds(priceInPence);
  }

  private String extractDescriptionFromDocument(final Document itemWebDocument) {
    final Element descriptionElement = itemWebDocument.select(GROCERY_ITEM_DESCRIPTION_DIV_NAME)
        .select(PARAGRAPH_TAG).first();
    return descriptionElement.text();
  }

  private double convertPenceToPounds(final double priceInPence) {
    return Precision.round(priceInPence / 100, 2);
  }
}
