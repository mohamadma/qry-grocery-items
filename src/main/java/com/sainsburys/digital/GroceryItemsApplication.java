package com.sainsburys.digital;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GroceryItemsApplication {

  public static void main(String[] args) {
    SpringApplication.run(GroceryItemsApplication.class);
  }

}
