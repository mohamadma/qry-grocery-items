package com.sainsburys.digital.util;

import com.sainsburys.digital.domain.GroceryItem;

/**
 * Test data factory for test classes. Be wary of changing values as there are dependencies between
 * objects.
 */
public class TestDataFactory {

  public static final String INPUT_URL = "http://www.test.com";
  public static final String GRID_ITEM_DIV_NAME = "li.GridItem";
  public static final double GROCERY_ITEM_UNIT_PRICE = 1.50;
  public static final String LINK_HTML_TAG = "a";
  public static final String ABSOLUTE_HREF_TAG = "abs:href";
  public static final String GROCERY_ITEM_TITLE_DIV_NAME = "div.productTitleDescriptionContainer";
  public static final String HEADING_TAG = "h1";
  public static final String GROCERY_ITEM_CALORIES_DIV_NAME = "td.nutritionLevel1";
  public static final String PRICE_PER_UNIT_DIV_NAME = "p.pricePerUnit";
  public static final String GROCERY_ITEM_DESCRIPTION_DIV_NAME = "div.productText";
  public static final String PARAGRAPH_TAG = "p";


  public static String createIntegrationTestExpectedJsonResponseString() {
    return "{\"results\":[{\"title\":\"Sainsbury's Strawberries 400g\",\"kcal_per_100g\":33,\"unit_price\":1.75,\"description\":\"by Sainsbury's strawberries\"},{\"title\":\"Sainsbury's Blueberries 200g\",\"unit_price\":1.75,\"description\":\"by Sainsbury's blueberries\"},{\"title\":\"Sainsbury's Raspberries 225g\",\"unit_price\":1.75,\"description\":\"by Sainsbury's raspberries\"},{\"title\":\"Sainsbury's Blackberries, Sweet 150g\",\"kcal_per_100g\":32,\"unit_price\":1.5,\"description\":\"by Sainsbury's blackberries\"},{\"title\":\"Sainsbury's Blueberries 400g\",\"kcal_per_100g\":45,\"unit_price\":3.25,\"description\":\"by Sainsbury's blueberries\"},{\"title\":\"Sainsbury's Blueberries, SO Organic 150g\",\"unit_price\":2.0,\"description\":\"So Organic blueberries\"},{\"title\":\"Sainsbury's Raspberries, Taste the Difference 150g\",\"unit_price\":2.5,\"description\":\"Ttd raspberries\"},{\"title\":\"Sainsbury's Cherries 400g\",\"unit_price\":2.5,\"description\":\"by Sainsbury's Family Cherry Punnet\"},{\"title\":\"Sainsbury's Blackberries, Tangy 150g\",\"unit_price\":1.5,\"description\":\"by Sainsbury's blackberries\"},{\"title\":\"Sainsbury's Strawberries, Taste the Difference 300g\",\"kcal_per_100g\":27,\"unit_price\":2.5,\"description\":\"Ttd strawberries\"},{\"title\":\"Sainsbury's Cherry Punnet 200g\",\"unit_price\":1.5,\"description\":\"Cherries\"},{\"title\":\"Sainsbury's Mixed Berries 300g\",\"unit_price\":3.5,\"description\":\"by Sainsbury's mixed berries\"},{\"title\":\"Sainsbury's Mixed Berry Twin Pack 200g\",\"unit_price\":2.75,\"description\":\"Mixed Berries\"},{\"title\":\"Sainsbury's Redcurrants 150g\",\"unit_price\":2.5,\"description\":\"by Sainsbury's redcurrants\"},{\"title\":\"Sainsbury's Cherry Punnet, Taste the Difference 200g\",\"unit_price\":2.5,\"description\":\"Cherry Punnet\"},{\"title\":\"Sainsbury's Blackcurrants 150g\",\"unit_price\":1.75,\"description\":\"\"},{\"title\":\"Sainsbury's British Cherry & Strawberry Pack 600g\",\"unit_price\":4.0,\"description\":\"British Cherry & Strawberry Mixed Pack\"}],\"total\":39.5}";
  }

  public static GroceryItem createGroceryItemWithAllAttributes() {
    return new GroceryItem("Berries", 100, 1.50, "tasty");
  }

  public static String createCaloriesText() {
    return "random string containing the string 100kcal to parse the number of calories in an item";
  }

  public static String createPriceText() {
    return "random string containing the string £1.50 to parse the price of an item";
  }

  public static String createDescriptionText() {
    return "tasty";
  }


}
