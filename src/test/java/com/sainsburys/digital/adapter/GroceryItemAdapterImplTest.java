package com.sainsburys.digital.adapter;

import static com.sainsburys.digital.util.TestDataFactory.ABSOLUTE_HREF_TAG;
import static com.sainsburys.digital.util.TestDataFactory.GROCERY_ITEM_CALORIES_DIV_NAME;
import static com.sainsburys.digital.util.TestDataFactory.GROCERY_ITEM_DESCRIPTION_DIV_NAME;
import static com.sainsburys.digital.util.TestDataFactory.GROCERY_ITEM_TITLE_DIV_NAME;
import static com.sainsburys.digital.util.TestDataFactory.HEADING_TAG;
import static com.sainsburys.digital.util.TestDataFactory.INPUT_URL;
import static com.sainsburys.digital.util.TestDataFactory.LINK_HTML_TAG;
import static com.sainsburys.digital.util.TestDataFactory.PARAGRAPH_TAG;
import static com.sainsburys.digital.util.TestDataFactory.PRICE_PER_UNIT_DIV_NAME;
import static com.sainsburys.digital.util.TestDataFactory.createCaloriesText;
import static com.sainsburys.digital.util.TestDataFactory.createDescriptionText;
import static com.sainsburys.digital.util.TestDataFactory.createGroceryItemWithAllAttributes;
import static com.sainsburys.digital.util.TestDataFactory.createPriceText;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import com.sainsburys.digital.domain.GroceryItem;
import java.io.IOException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Jsoup.class)
public class GroceryItemAdapterImplTest {

  private GroceryItemAdapterImpl testClass;

  @Mock
  private Element mockElement;

  private GroceryItem actualOutput;
  private GroceryItem expectedOutput = createGroceryItemWithAllAttributes();

  @Test
  public void shouldConvertElementToGroceryItem() throws IOException {
    givenAGroceryItemAdapterImpl();
    andTheMockInputIsConfigureToReturnAllAttributes();
    whenElementToGroceryItemListIsInvoked();
    thenTheOutputShouldContainAllAttributes();
  }

  private void givenAGroceryItemAdapterImpl() {
    testClass = new GroceryItemAdapterImpl();
  }

  private void andTheMockInputIsConfigureToReturnAllAttributes() throws IOException {
    mockStatic(Jsoup.class);
    Elements mockElements = mock(Elements.class);
    Connection mockConnection = mock(Connection.class);
    Document mockDocument = mock(Document.class);

    when(mockElement.select(LINK_HTML_TAG)).thenReturn(mockElements);
    when(mockElements.attr(ABSOLUTE_HREF_TAG)).thenReturn(INPUT_URL);
    when(Jsoup.connect(INPUT_URL)).thenReturn(mockConnection);
    when(mockConnection.get()).thenReturn(mockDocument);

    // Get item title
    Elements mockTitleElements = mock(Elements.class);
    Element mockTitleElement = mock(Element.class);
    when(mockDocument.select(GROCERY_ITEM_TITLE_DIV_NAME)).thenReturn(mockElements);
    when(mockElements.select(HEADING_TAG)).thenReturn(mockTitleElements);
    when(mockTitleElements.first()).thenReturn(mockTitleElement);
    when(mockTitleElement.text()).thenReturn(expectedOutput.getTitle());

    // Get item calories
    Elements mockCaloriesElements = mock(Elements.class);
    when(mockDocument.select(GROCERY_ITEM_CALORIES_DIV_NAME)).thenReturn(mockCaloriesElements);
    when(mockCaloriesElements.text()).thenReturn(createCaloriesText());

    // Get item price
    Elements mockPriceElements = mock(Elements.class);
    Element mockPriceElement = mock(Element.class);
    when(mockDocument.select(PRICE_PER_UNIT_DIV_NAME)).thenReturn(mockPriceElements);
    when(mockPriceElements.first()).thenReturn(mockPriceElement);
    when(mockPriceElement.text()).thenReturn(createPriceText());

    // Get item description
    Elements mockDescriptionElements = mock(Elements.class);
    Element mockDescriptionElement = mock(Element.class);
    when(mockDocument.select(GROCERY_ITEM_DESCRIPTION_DIV_NAME)).thenReturn(mockDescriptionElements);
    when(mockDescriptionElements.select(PARAGRAPH_TAG)).thenReturn(mockDescriptionElements);
    when(mockDescriptionElements.first()).thenReturn(mockDescriptionElement);
    when(mockDescriptionElement.text()).thenReturn(createDescriptionText());
  }

  private void whenElementToGroceryItemListIsInvoked() {
    actualOutput = testClass.elementToGroceryItemList(mockElement);
  }

  private void thenTheOutputShouldContainAllAttributes() {
    assertThat(actualOutput).isEqualToComparingFieldByField(expectedOutput);
  }

}