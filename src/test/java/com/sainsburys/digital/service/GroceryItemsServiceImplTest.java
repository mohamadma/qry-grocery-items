package com.sainsburys.digital.service;

import static com.sainsburys.digital.util.TestDataFactory.GRID_ITEM_DIV_NAME;
import static com.sainsburys.digital.util.TestDataFactory.GROCERY_ITEM_UNIT_PRICE;
import static com.sainsburys.digital.util.TestDataFactory.INPUT_URL;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import com.sainsburys.digital.adapter.GroceryItemAdapter;
import com.sainsburys.digital.domain.GroceryItem;
import com.sainsburys.digital.domain.GroceryItemsResponse;
import java.io.IOException;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(Jsoup.class)
public class GroceryItemsServiceImplTest {

  private GroceryItemsServiceImpl testClass;

  @Mock
  private GroceryItemAdapter mockGroceryItemAdapter;

  @Mock
  private GroceryItem mockGroceryItem;

  private GroceryItemsResponse actualOutput;

  @Test
  public void shouldReturnValidGroceryItemsResponse() throws IOException {
    givenAGroceryItemsServiceImplWithMockedServices();
    andTheMocksAreConfiguredToReturnSuccess();
    whenGetGroceryItemsFromUrlIsInvoked();
    thenAValidGroceryItemsResponseShouldBeReturned();
  }

  private void givenAGroceryItemsServiceImplWithMockedServices() {
    testClass = new GroceryItemsServiceImpl(mockGroceryItemAdapter);
  }

  private void andTheMocksAreConfiguredToReturnSuccess() throws IOException {
    Connection mockConnection = mock(Connection.class);
    Document mockDocument = mock(Document.class);
    Element mockElement = mock(Element.class);

    Elements elements = new Elements(){{
      add(mockElement);
      add(mockElement);
    }};

    mockStatic(Jsoup.class);
    when(Jsoup.connect(INPUT_URL)).thenReturn(mockConnection);
    when(mockConnection.get()).thenReturn(mockDocument);
    when(mockDocument.select(GRID_ITEM_DIV_NAME)).thenReturn(elements);
    when(mockGroceryItemAdapter.elementToGroceryItemList(any())).thenReturn(mockGroceryItem);
    when(mockGroceryItem.getUnitPrice()).thenReturn(GROCERY_ITEM_UNIT_PRICE);
  }

  private void whenGetGroceryItemsFromUrlIsInvoked() throws IOException {
    actualOutput = testClass.getGroceryItemsFromUrl(INPUT_URL);
  }

  private void thenAValidGroceryItemsResponseShouldBeReturned() {
    assertThat(actualOutput).isNotNull();
    assertThat(actualOutput.getGroceryItems().size()).isEqualToComparingFieldByField(2);
    assertThat(actualOutput.getGroceryItems().get(0)).isEqualTo(mockGroceryItem);
    assertThat(actualOutput.getGroceryItems().get(1)).isEqualTo(mockGroceryItem);
    assertThat(actualOutput.getTotalPrice()).isEqualTo(3.00);
  }


}