package com.sainsburys.digital;

import static com.sainsburys.digital.util.TestDataFactory.createIntegrationTestExpectedJsonResponseString;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.core.Is.isA;
import static org.hamcrest.core.IsEqual.equalTo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sainsburys.digital.controller.GroceryItemsController;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import java.io.IOException;
import java.util.List;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Tests the application end to end using the URL in main/java/resources/application.properties.
 * If the page the URL points to changes, or the URL itself changes then this test will break.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.DEFINED_PORT)
public class GroceryItemsApplicationIntegrationTest {

  private String url;
  private RequestSpecification requestSpecification;
  private ResponseSpecification responseSpecification;
  private Response actualResponse;
  private String jsonResponseString;

  @LocalServerPort
  private int port = 8080;

  @Autowired
  private GroceryItemsController groceryItemsController;

  @Test
  public void shouldReturnValidGroceryItemsResponse() throws IOException {
    givenAValidUrlEndpoint();
    andARequestSpecification();
    andAResponseSpecification();
    whenGetGroceryItemsIsInvoked();
    thenAValidResponseShouldBeReturned();
  }

  private void givenAValidUrlEndpoint() {
    url = "http://localhost:" + port + "/groceryItems";
  }

  private void andARequestSpecification() {
    requestSpecification = new RequestSpecBuilder()
        .setBaseUri(url)
        .build();
  }

  private void andAResponseSpecification() throws JsonProcessingException {
    responseSpecification = new ResponseSpecBuilder()
        .expectContentType(ContentType.JSON)
        .expectStatusCode(HttpStatus.OK.value())
        .expectBody("total", equalTo(39.5f))
        .expectBody("results", isA(List.class))
        .build();
  }

  private void whenGetGroceryItemsIsInvoked() {
    actualResponse = given()
        .spec(requestSpecification)
        .when()
        .get("/getAll");
    jsonResponseString = actualResponse.then().contentType(ContentType.JSON).extract().response().asString();
  }

  private void thenAValidResponseShouldBeReturned() throws JsonProcessingException {
    actualResponse.then()
        .spec(responseSpecification);
    assertThat(jsonResponseString).isEqualTo(createIntegrationTestExpectedJsonResponseString());
  }

}
