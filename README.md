# Qry-Grocery-Items

##Introduction
This is a Spring Boot application that queries the Sainsbury's groceries website and scrapes the data
on it to return each Grocery Item's name, description, price, number of calories per 100g and the total
price of all the items.

##Usage
To start the application, run the main method in GroceryItemsApplication and use a tool such as Postman
or Insomnia to fire a GET request to:

    localhost:8080/products/groceryItems/
No headers or a JSON body is required.
This will return the all of the grocery items in JSON format that are on the URL in the 
application.properties file.

##Notes the for examiner
The past couple of weeks have been a little crazy at work with regards to deadlines and so my commitment
to this project has been scattered across several sittings as I've had to squeeze it into whatever free
time I had left. Therefore, there are several features I would've liked to have implemented given
the extra time including:


- Better exception handling using @ControllerAdvice and @ExceptionHandler to catch 404 errors and 500 errors
and return a more appropriate response.


- More comprehensive unit and integration tests- the current ones only cover the bare minimum and don't handle
cases where exceptions may be thrown.


- Better version control- under normal circumstances, I would implement each feature separately on 
a feature branch and merge it to master once it was ready however due to the ad-hoc nature of my coding this wouldn't
have been feasible.


- A test-driven approach. Under normal circumstances, I would write my tests first however, again due
to time constraints this wasn't viable.


- Deploying the application on AWS Elastic Beanstalk.


- Implementing a search endpoint in the controller. This would've taken a search term as a path variable
and returned the grocery items that contained the search term in their title or description, rather 
than all of them.